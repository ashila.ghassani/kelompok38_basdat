from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('', create_sesi_konsul, name='rud_sesi_konsul'),
    path('rud_sesi_konsul/', rud_sesi_konsul, name='rud_sesi_konsul'),
    path('', rud_sesi_konsul, name='rud_sesi_konsul'),
    path('update_sesi_konsul/', update_sesi_konsul, name='update_sesi_konsul'),
]
