from django.shortcuts import render

# Create your views here.

def rud_sesi_konsul(request):
    argumen = {}
    return render(request, 'RUD_sesi_konsultasi.html', argumen)

def update_sesi_konsul(request):
    argumen = {}
    return render(request, 'update_sesi_konsultasi.html', argumen)

def create_sesi_konsul(request):
    argumen = {}
    return render(request, 'create_sesi_konsultasi.html', argumen)
