from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormLayananPoli, FormLP, FormJadwal, JadwalFormset
from django.forms import formset_factory
from django.db import connection

# Create your views here.
def lp(request):
    LPFormSet = formset_factory(FormLayananPoli, extra=1)
    formset = LPFormSet(initial=[])

    if request.method=="POST":
        item_formset = LPFormSet(request.POST)
        listLP = []
        listJadwal = []
        if item_formset.is_valid():
            for f in item_formset:
                if f.cleaned_data.get('nama'):
                    listLP.append(f.cleaned_data.get('nama'))
                if f.cleaned_data.get('deskripsi'):
                    listLP.append(f.cleaned_data.get('deskripsi'))
                if f.cleaned_data.get('kode_rs_cabang'):
                    listLP.append(f.cleaned_data.get('kode_rs_cabang'))
                    
                if f.cleaned_data.get('id_dokter'):
                    listJadwal.append(f.cleaned_data.get('id_dokter'))
                if f.cleaned_data.get('hari'):
                    listJadwal.append(f.cleaned_data.get('hari'))
                if f.cleaned_data.get('waktu_mulai'):
                    listJadwal.append(f.cleaned_data.get('waktu_mulai'))
                if f.cleaned_data.get('waktu_selesai'):
                    listJadwal.append(f.cleaned_data.get('waktu_selesai'))
                if f.cleaned_data.get('kapasitas'):
                    listJadwal.append(f.cleaned_data.get('kapasitas'))
                
                FormLayananPoli.saveLP(listLPI=listLP)    
                FormLayananPoli.saveJadwal(listJadwalI=listJadwal)
                listLP.clear()
                listJadwal.clear()
            return redirect("datalp")               
        
    else:
        argument= {}
        argument['formset']= LPFormSet

    return render(request,'lynpoli.html', argument)

def datalp(request):
    asd = ''
    with connection.cursor() as dokter:
        dokter.execute("SELECT * FROM LAYANAN_POLIKLINIK ORDER BY id_poliklinik ASC")
        asd = dokter.fetchall()
    
    return render(request, 'datalayanan.html',{'asd' : asd})

def jadwallp(request):
    asd = ''
    with connection.cursor() as dokter:
        dokter.execute("SELECT * FROM JADWAL_LAYANAN_POLIKLINIK ORDER BY id_jadwal_poliklinik ASC")
        asd = dokter.fetchall()
    
    return render(request, 'datajadwal.html',{'asd' : asd})

def jadwalperpoli(request):
    asd = ''
    if request.method == 'POST':
        idpolnya = request.POST.get('idpol')
        if idpolnya:
            with connection.cursor() as dokter:
                dokter.execute('select * from jadwal_layanan_poliklinik where id_poliklinik=%s', [idpolnya])
                asd = dokter.fetchall()

    return render(request, 'jadwalperpoli.html',{'asd' : asd})

def updatelp(request):
    if request.method == 'GET':
        return redirect('datalp')
    form = FormLP
    if request.method == 'POST':
        
        if request.POST.get('edit') == 'edit_lp':
            idpolnya = request.POST.get('idpol')
            with connection.cursor() as lp:
                lp.execute('select * from layanan_poliklinik where id_poliklinik=%s', [idpolnya])
                lp = lp.fetchone()
                
                dct_of_lp = {
                    'id_poliklinik' : lp[0],
                    'kode_rs_cabang' : lp[1],
                    'nama' : lp[2],
                    'deskripsi' : lp[3],
                }

                form = form(dct_of_lp)

                return render(request, 'updatelp.html', {'form' : form, 'idlp' : lp[0]})
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_poliklinik=request.POST.get('idlp'))
                return redirect('datalp')
    return render(request, 'updatelp.html', {'form' : form})

def deletelp(request):
    if request.method == 'POST':
        idlpnya = request.POST.get('idpol')
        if idlpnya:
            with connection.cursor() as admin:
                admin.execute('delete from jadwal_layanan_poliklinik where id_poliklinik=%s', [idlpnya])
                admin.execute('delete from layanan_poliklinik where id_poliklinik=%s', [idlpnya])
    return redirect('datalp')

def updatejadwal(request):
    if request.method == 'GET':
        return redirect('jadwallp')
    form = FormJadwal
    if request.method == 'POST':
        
        if request.POST.get('edit') == 'edit_jadwal':
            idjadwalnya = request.POST.get('idjadwal')
            with connection.cursor() as lp:
                lp.execute('select * from jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s', [idjadwalnya])
                lp = lp.fetchone()
                
                dct_of_lp = {
                    'id_jadwal_poliklinik' : lp[0],
                    'hari' : lp[3],
                    'waktu_mulai' : lp[1],
                    'waktu_selesai' : lp[2],
                    'kapasitas' : lp[4],
                    'id_dokter' : lp[5],
                    'id_poliklinik' : lp[6],
                }

                form = form(dct_of_lp)

                return render(request, 'updatejadwal.html', {'form' : form, 'idjwl' : lp[0]})
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_jadwal_poliklinik=request.POST.get('idjwl'))
                return redirect('jadwallp')
    return render(request, 'updatejadwal.html', {'form' : form})

def deletejadwal(request):
    if request.method == 'POST':
        idjadwalnya = request.POST.get('idjadwal')
        if idjadwalnya:
            with connection.cursor() as admin:
                admin.execute('delete from jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s', [idjadwalnya])
    return redirect('jadwallp')


#{'hari': 'Sunday',
#'waktu_mulai': '09:09',
#'waktu_selesai': '12:12',
#'kapasitas' : '10',
#},
# {'hari': 'Sunday',
# 'waktu_mulai': '16:09',
# 'waktu_selesai': '19:12',
# 'kapasitas' : '9',
# },
# {'hari': 'Monday',
# 'waktu_mulai': '10:09',
# 'waktu_selesai': '13:12',
# 'kapasitas' : '13',
# },
# {'hari': 'Monday',
# 'waktu_mulai': '10:09',
# 'waktu_selesai': '13:12',
# 'kapasitas' : '16',
# },
# {'hari': 'Tuesday',
# 'waktu_mulai': '11:09',
# 'waktu_selesai': '14:12',
# 'kapasitas' : '14',
# },
# {'hari': 'Wednesday',
# 'waktu_mulai': '12:09',
# 'waktu_selesai': '15:12',
# 'kapasitas' : '20',
# },
# {'hari': 'Thursday',
# 'waktu_mulai': '13:09',
# 'waktu_selesai': '16:12',
# 'kapasitas' : '14',
# },
# {'hari': 'Friday',
# 'waktu_mulai': '14:09',
# 'waktu_selesai': '17:12',
# 'kapasitas' : '11',
# },
# {'hari': 'Saturday',
# 'waktu_mulai': '15:09',
# 'waktu_selesai': '18:12',
# 'kapasitas' : '10',
# },