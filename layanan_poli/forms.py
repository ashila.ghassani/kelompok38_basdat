from django import forms
from django.forms import widgets
from datetime import datetime
from django.db import connection
from hashlib import sha3_512 as sha
from django.forms import formset_factory

class FormLayananPoli(forms.Form):
    nama =forms.CharField(
        label = 'Nama Layanan', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Nama Layanan"}),
    )

    deskripsi =forms.CharField(
        label = 'Deskripsi', max_length = 250, required = False,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Deskripsi Layanan",}),
    )

    kode_rs_cabang = forms.ChoiceField(
        label='Kode RS',
        choices=[('101', '101'),
                ('102', '102'),
                ('103', '103'),
                ('104', '104'),
                ('105', '105')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    id_dokter = forms.ChoiceField(
        label='ID Dokter',
        choices=[('1', '1'),
                ('2', '2'),
                ('3', '3'),
                ('4', '4'),
                ('5', '5'),
                ('6', '6'),
                ('7', '7')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    hari = forms.CharField(label = 'Hari', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_mulai = forms.TimeField(label = 'Waktu Mulai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_selesai = forms.TimeField(label = 'Waktu Selesai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    kapasitas = forms.IntegerField(label = 'Kapasitas', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    def saveLP(commit=True, listLPI=list):

        jumlah=0
        with connection.cursor() as hitung:
            hitung.execute("SELECT COUNT(*) FROM LAYANAN_POLIKLINIK")
            jumlah += hitung.fetchone()[0]

        id_poliklinik = 0
        if jumlah < 1:
            id_poliklinik = 111
        elif jumlah >=1:
            id_poliklinik = 111 + jumlah
        
        if len(listLPI)<3:
            nama = listLPI[0]
            deskripsi = None
            kode_rs_cabang = listLPI[1]

        if len(listLPI)==3:
            nama = listLPI[0]
            deskripsi = listLPI[1]
            kode_rs_cabang = listLPI[2]

        with connection.cursor() as dokter:
            dokter.execute('insert into layanan_poliklinik values (%s, %s, %s, %s)', [id_poliklinik, kode_rs_cabang, nama, deskripsi])

    def saveJadwal(commit=True, listJadwalI=list):

        jumlah=0
        with connection.cursor() as hitung:
            hitung.execute("SELECT COUNT(*) FROM JADWAL_LAYANAN_POLIKLINIK")
            jumlah += hitung.fetchone()[0]
        
        id_poliklinik=''
        with connection.cursor() as hitung:
            hitung.execute("SELECT id_poliklinik FROM LAYANAN_POLIKLINIK ORDER BY id_poliklinik DESC LIMIT 1")
            id_poliklinik = hitung.fetchone()

        id_jadwal_poliklinik = 0
        if jumlah < 1:
            id_jadwal_poliklinik = 1
        elif jumlah >=1:
            id_jadwal_poliklinik = 1 + jumlah
        waktu_mulai = listJadwalI[2]
        waktu_selesai = listJadwalI[3]
        hari = listJadwalI[1]
        kapasitas = listJadwalI[4]
        id_dokter = listJadwalI[0]

        with connection.cursor() as dokter:
            dokter.execute('insert into jadwal_layanan_poliklinik values (%s, %s, %s, %s, %s, %s, %s)', [id_jadwal_poliklinik, waktu_mulai, waktu_selesai, hari, kapasitas, id_dokter, id_poliklinik])

    class Meta:
        # model = LayananPoli
        fields= ('id_poliklinik', 'id_jadwal_poliklinik', 'nama','deskripsi','kode_rs_cabang', 'id_dokter', 'hari', 'waktu_mulai', 'waktu_selesai', 'kapasitas')

class FormJadwalPoli(forms.Form):
    hari = forms.CharField(label = 'Hari', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_mulai = forms.TimeField(label = 'Waktu Mulai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_selesai = forms.TimeField(label = 'Waktu Selesai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    kapasitas = forms.IntegerField(label = 'Kapasitas', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    def save(self, commit=True):

        id_poliklinik = self.cleaned_data.get('id_poliklinik')
        kode_rs_cabang = self.cleaned_data.get('kode_rs_cabang')
        nama = self.cleaned_data.get('nama')
        deskripsi = self.cleaned_data.get('deskripsi')

        with connection.cursor() as dokter:
            dokter.execute('insert into layanan_poliklinik values (%s, %s, %s, %s)', [id_poliklinik, kode_rs_cabang, nama, deskripsi])

    class Meta:
        # model = LayananPoli
        fields= ('id_poliklinik', 'id_jadwal_poliklinik', 'nama','deskripsi','kode_rs_cabang', 'id_dokter', 'hari', 'waktu_mulai', 'waktu_selesai', 'kapasitas')

JadwalFormset = formset_factory(FormJadwalPoli, extra=1)

class FormLP(forms.Form):
    id_poliklinik = forms.CharField(
        label='ID Poliklinik',max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'autocomplete':'off', 'readonly':'true'}),
    )

    nama =forms.CharField(
        label = 'Nama Layanan', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Nama Layanan"}),
    )

    deskripsi =forms.CharField(
        label = 'Deskripsi', max_length = 250, required = False,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Deskripsi Layanan",}),
    )

    kode_rs_cabang = forms.ChoiceField(
        label='Kode RS',
        choices=[('101', '101'),
                ('102', '102'),
                ('103', '103'),
                ('104', '104'),
                ('105', '105')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    def edit(self, commit=True, id_poliklinik=None):
        if not (id_poliklinik):
            return

        kode_rsc = self.cleaned_data.get('kode_rs_cabang')
        name = self.cleaned_data.get('nama')
        desc = self.cleaned_data.get('deskripsi')

        with connection.cursor() as dokter:
            dokter.execute('update layanan_poliklinik set kode_rs_cabang =%s, nama = %s, deskripsi = %s where id_poliklinik=%s', [kode_rsc, name, desc, id_poliklinik])

    class Meta:
        # model = LayananPoli
        fields= ('id_poliklinik', 'nama','deskripsi','kode_rs_cabang','hari', 'waktu_mulai', 'waktu_selesai', 'kapasitas')

class FormJadwal(forms.Form):

    id_jadwal_poliklinik = forms.CharField(
        label='ID Jadwal Layanan Poliklinik',max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'autocomplete':'off', 'readonly':'true'}),
    )

    id_dokter = forms.ChoiceField(
        label='ID Dokter',
        choices=[('1', '1'),
                ('2', '2'),
                ('3', '3'),
                ('4', '4'),
                ('5', '5'),
                ('6', '6'),
                ('7', '7')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    hari = forms.CharField(label = 'Hari', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_mulai = forms.TimeField(label = 'Waktu Mulai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    waktu_selesai = forms.TimeField(label = 'Waktu Selesai', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    kapasitas = forms.IntegerField(label = 'Kapasitas', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),)

    id_poliklinik = forms.CharField(
        label='ID Poliklinik',max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'autocomplete':'off', 'readonly':'true'}),
    )

    def edit(self, commit=True, id_jadwal_poliklinik=None):
        if not (id_jadwal_poliklinik):
            return

        hari1 = self.cleaned_data.get('hari')
        w_mulai = self.cleaned_data.get('waktu_mulai')
        w_selesai = self.cleaned_data.get('waktu_selesai')
        kpts = self.cleaned_data.get('kapasitas')
        idd = self.cleaned_data.get('id_dokter')

        with connection.cursor() as dokter:
            dokter.execute('update jadwal_layanan_poliklinik set hari =%s, waktu_mulai = %s, waktu_selesai = %s, kapasitas = %s, id_dokter = %s where id_jadwal_poliklinik=%s', [hari1, w_mulai, w_selesai, kpts, idd, id_jadwal_poliklinik])

    class Meta:
        # model = LayananPoli
        fields= ('id_poliklinik', 'id_jadwal_poliklinik', 'id_dokter, hari, waktu_mulai, waktu_selesai, kapasitas')


