from django.apps import AppConfig


class CreateResepConfig(AppConfig):
    name = 'create_resep'
