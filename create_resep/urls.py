from django.urls import path
from . import views

#url for app

app_name = 'create_resep'

urlpatterns = [
    path('',views.createResep, name='createResep'),
    path('daftarResep/', views.daftarResep, name='daftarResep'),
    path('deleteResep/', views.deleteResep, name='delete')
]