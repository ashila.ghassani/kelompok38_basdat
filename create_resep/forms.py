from django import forms
from django.forms import widgets
from django.db import connection
import random
class FormResep(forms.Form):
    id_konsultasi = forms.ChoiceField(
        label = 'ID Konsultasi',
        choices=[('401', '401'),
                ('402', '402'),
                ('403', '403'),
                ('404', '404'),
                ('405', '405'),
                ('406', '406'),
                ('407', '407'),
                ('408', '408'),
                ('409', '409'),
                ('410', '410'),

                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})

    )
    id_transaksi = forms.ChoiceField(
        label = 'ID Transaksi',
        choices = [
                ('1001', '1001'),
                ('1002', '1002'),
                ('1003', '1003'),
                ('1004', '1004'),
                ('1005', '1005'),
                ('1006', '1006'),
                ('1007', '1007'),
                ('1008', '1008'),
                ('1009', '1009'),
                ('1010', '1010'),
        ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )
    kode_obat = forms.MultipleChoiceField(
        label = 'Daftar Kode Obat',
        choices = [('DCLGN','DCLGN'),
                ('OBH','OBH'),
                ('PNDL','PNDL'),
                ('TLKANGN','TLKANGN'),],
        widget = forms.CheckboxSelectMultiple(attrs = {'class' : 'form-control'})
    )
    
    def save(self, commit=True):
        id_konsultasi = self.cleaned_data.get('id_konsultasi')
        id_transaksi = self.cleaned_data.get('id_transaksi')
        kode_obat = self.cleaned_data.get('kode_obat')
        dosis = ['1','2','3''4','5']
        aturan_pakai = ['sebelum makan','sesudah makan']


    
        with connection.cursor() as cursor: 
            cursor.execute(
                "SELECT MAX(no_resep) from RESEP;"
            )
            data = cursor.fetchone()
            no_resep = int(data[0]) + 1
            print(no_resep)

            cursor.execute(
                "INSERT INTO RESEP(no_resep, id_konsultasi, total_harga, id_transaksi) VALUES('%s', '%s', %d, '%s');" % (no_resep, id_konsultasi, 0, id_transaksi)
            )
            for kode in kode_obat:
                cursor.execute(
                    "INSERT INTO DAFTAR_OBAT(no_resep, kode_obat, dosis, aturan) VALUES('%s', '%s', '%s', '%s');" % (no_resep, kode, random.choice(dosis), random.choice(aturan_pakai))
                )
        
    class Meta:
        # model = Resep
        fields= ('id_konsultasi','id_transaksi','kode_obat')
      