from django.shortcuts import render,redirect
from .forms import FormResep
from django.http import HttpResponseRedirect
from django.db import connection

# Create your views here.
def createResep(request):
    form = FormResep
    if request.method=="POST":
        form= form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('create_resep:daftarResep')            
            
    return render(request,'createResep.html', {'form' : form})

def daftarResep(request):
    daftar_resep = ''
    with connection.cursor() as resep:
        resep.execute("SELECT R.no_resep, R.id_konsultasi, R.total_harga, R.id_transaksi,"
        "STRING_AGG(D.kode_obat,',') as daftar_obat "
        "FROM RESEP R, DAFTAR_OBAT D "
        "WHERE R.no_resep = D.no_resep "
        "GROUP BY R.no_resep "
        "ORDER BY R.no_resep;")

        daftar_resep = resep.fetchall()

        return render(request, 'daftarResep.html',{'daftar_resep' : daftar_resep})
    
def deleteResep(request):
    if request.method == 'POST':
        no_resep = request.POST.get('no_resep')
        with connection.cursor() as admin:
            admin.execute("DELETE FROM RESEP WHERE no_resep=%s",[no_resep])
    return redirect('create_resep:daftarResep')
