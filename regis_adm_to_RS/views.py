from django.shortcuts import render,redirect
from .forms import FormAdmToRS
from django.http import HttpResponseRedirect


# Create your views here.
def regisAdm(request):
    form= FormAdmToRS
    if request.method=="POST":
        form= form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('rs_cabang:rud_rs_cabang')   

    return render(request,'registerAdm.html', {'form' : form})


