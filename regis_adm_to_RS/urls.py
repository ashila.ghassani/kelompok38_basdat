from django.urls import path
from . import views
from rs_cabang import views as views2


#url for app

app_name = 'rs_cabang'

urlpatterns = [
    path('',views.regisAdm, name='regisAdm'),
    path('rud_rs_cabang/', views2.rud_rs_cabang, name='rud_rs_cabang')
]

