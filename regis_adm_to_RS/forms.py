from django import forms
from django.forms import widgets
from django.db import connection

class FormAdmToRS(forms.Form):
    no_pegawai = forms.ChoiceField(
        label = 'Nomor Pegawai',
        choices=[('1', '1'),
                ('2', '2'),
                ('3', '3'),
                ('4', '4'),
                ('5', '5')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})

    )
    kode_rs = forms.ChoiceField(
        label = 'Kode RS',
        choices = [('101','101'),
                ('102','102'),
                ('103','103'),
                ('104','104'),
                ('105','105')
        ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    def save(self, commit=True):

        no_pegawai = self.cleaned_data.get('no_pegawai')
        kode_rs = self.cleaned_data.get('kode_rs')

        with connection.cursor() as adm:
            adm.execute('insert into administrator_rs_cabang values (%s, %s)', [no_pegawai, kode_rs])


    
    class Meta:
        # model = Administrastor
        fields= ('no_pegawai','kode_rs')
      