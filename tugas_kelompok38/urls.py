"""tugas_kelompok38 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('login.urls')),
    path('regisdokterrs/', include('reg_dokter_rs.urls')),
    path('layananpoli/', include('layanan_poli.urls')),
    path('transaksi/', include('transaksi.urls')),
    path('registerAdm/', include('regis_adm_to_RS.urls')),
    path('createResep/', include('create_resep.urls')),
    path('createObat/', include('create_obat.urls')),
    path('sesi_konsul/', include('sesi_konsultasi.urls')),
    path('rs_cabang/', include('rs_cabang.urls')),
]
