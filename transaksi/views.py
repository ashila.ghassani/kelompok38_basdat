from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .forms import TransaksiForm, createtrans
from django.db import connection
from collections import namedtuple
import datetime


 
# Get current date and time
dt = datetime.datetime.now()
 
# Format datetime string
x = dt.strftime("%Y-%m-%d %H:%M:%S")
 

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def readtransaksi(request):
    data = ''
    with connection.cursor() as transaksi:
        transaksi.execute("SELECT * FROM TRANSAKSI ORDER BY id_transaksi ASC")
        data = transaksi.fetchall()

    return render(request, 'daftar_transaksi.html', {'data' : data})


def updatetransaksi(request, id):
    if request.method == 'POST':
        data = ''
        with connection.cursor() as transaksi:
            transaksi.execute("SELECT * from transaksi WHERE id_transaksi = %s", [id] )
            data = transaksi.fetchall()

            id_transaksi = request.POST.get("id_transaksi")
            tanggal = request.POST.get("tanggal")
            status = request.POST.get("status")
            total_biaya = request.POST.get("total_biaya")
            waktu_pembayaran = request.POST.get("waktu_pembayaran")
            no_rekam_medis = request.POST.get("no_rekam_medis")

            try:
        
                transaksi.execute('UPDATE transaksi SET tanggal = %s, status = %s, waktu_pembayaran = %s WHERE id_transaksi = %s',[tanggal,status,waktu_pembayaran,id])
            except utils.IntegrityError as e:
                return render(request, "daftar_transaksi.html", {"message" : str(e)})
                
            return redirect("readtransaksi")

            
    if request.method == 'GET':
        data = ''
        with connection.cursor() as transaksi:
            transaksi.execute("SELECT id_transaksi,total_biaya, no_rekam_medis FROM TRANSAKSI WHERE id_transaksi = %s", [id])
            data = transaksi.fetchall()[0]

            

        return render(request, 'update_transaksi.html', {'data' : data})




def createtransaksi(request):
    if request.method  == 'POST':
        data = ''
        with connection.cursor() as transaksi:
            transaksi.execute("SELECT id_transaksi FROM transaksi ORDER BY id_transaksi DESC LIMIT 1")
            id_transaksi = transaksi.fetchone()[0]
            id_transaksi = str(int(id_transaksi) + 1)
            print(id_transaksi)
                
            transaksi.execute("SELECT CURRENT_DATE")
            tanggal = transaksi.fetchone()
                
            status = 'Created'
            
            total_biaya = 0
                
            dt = datetime.datetime.now()
            waktu_pembayaran = dt.strftime("%Y-%m-%d %H:%M:%S")
                
            no_rekam_medis = request.POST.get("no_rekam_medis")

            query = "INSERT INTO transaksi(id_transaksi, tanggal, status, total_biaya, waktu_pembayaran, no_rekam_medis) VALUES(%s, %s, %s, %s, %s, %s); "
    
            try:
        
                transaksi.execute(query, [id_transaksi, tanggal, status, total_biaya, waktu_pembayaran, no_rekam_medis])
            except utils.IntegrityError as e:
                return render(request, "daftar_transaksi.html", {"message" : str(e)})
                
            return redirect("readtransaksi")
    else:
        data = ''
        with connection.cursor() as transaksi:
            transaksi.execute("SELECT DISTINCT no_rekam_medis FROM PASIEN ORDER BY no_rekam_medis ASC")
            data = transaksi.fetchall()
        return render(request, "create_transaksi.html", {"data" : data })


def deletetransaksi(request):
    if request.method == 'POST':
        id_transaksi = request.POST.get('id_transaksi')
        if id_transaksi:
            with connection.cursor() as transaksi:
                transaksi.execute('delete from transaksi where id_transaksi=%s', [id_transaksi])

                

    return redirect("readtransaksi")
    # data = ''
    # with connection



    
    # create_trans = createtrans()
    # context = {
    #     'create': create_trans

    # }
    # # data = ''

    # # with connection.cursor() as transaksi:
    # #     transaksi.execute("INSERT INTO TRANSAKSI VALUES ()")
    # #     data = transaksi.fetchall()
    # return render(request,'create_transaksi.html', context)




    


 
     