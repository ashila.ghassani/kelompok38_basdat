from django.urls import include, path
from .views import readtransaksi, updatetransaksi, createtransaksi, deletetransaksi

urlpatterns = [
    path('', readtransaksi, name='readtransaksi'),
    path('update/<str:id>', updatetransaksi, name='updatetransaksi'),
    path('create/', createtransaksi, name = 'createtransaksi'),
    path('delete', deletetransaksi, name = 'deletetransaksi'),
]
