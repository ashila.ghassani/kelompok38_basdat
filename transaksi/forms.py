from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'


class TransaksiForm(forms.Form):
    ID_Transaksi = forms.IntegerField(disabled = True, initial = '1')
    Tanggal = forms.DateField(widget = DateInput)
    Status = forms.CharField(max_length=100)
    Total_Biaya = forms.CharField(disabled = True, initial = 'Rp250000')
    Waktu_Pembayaran = forms.DateField(widget = DateInput)
    Nomor_rekam_medis_pasien = forms.IntegerField(disabled = True, initial = '1')


# iterable 
CHOICES =( 
    ("1", "1"), 
    ("2", "2"), 
    ("3", "3"), 
    ("4", "4"), 
    ("5", "5"), 
) 
  
# creating a form  
class createtrans(forms.Form): 
    Nomor_rekam_medis = forms.ChoiceField(choices = CHOICES)