# Create your views here.
from django.shortcuts import render, redirect
#from django.core import serializers
#from .models import loginMedikaGo
#from django.contrib import messages
#from django.contrib.auth.forms import AuthenticationForm
#from django.contrib.auth import authenticate, login
#from .forms import registerForm
# import requests
from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from hashlib import sha3_512 as sha
from .forms import DokterForm, AdminForm, PasienForm, LoginForm
from .decorators import admin_only, authors_only

# Create your views here.
def logout(request):
    request.session.flush()
    return redirect('login:fiturlogin')

def home(request):
    return render(request,'home.html')
    
def fiturlogin(request):
    form = LoginForm
    if request.session.get('role') == 'pasien' or request.session.get('role') == 'dokter' or request.session.get('role') == 'admin':
        return redirect('login:home')
    if request.method == 'POST':
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        role = request.POST.get('role', False)
        if not (username and password):
            return redirect('home')

        # hash password with sha3
        # hashed_password = sha(password.encode()).hexdigest()
        with connection.cursor() as login_cursor:
            login_cursor.execute("select * from pengguna where username=%s and password=%s", [username, password])
            result = login_cursor.fetchone()
            if result:
                if role == 'admin':
                    with connection.cursor() as adm:
                        adm.execute("select * from administrator where username=%s", [username])
                        hasil = adm.fetchone()
                        if hasil:
                            request.session['role'] = role
                            return redirect('login:home')

                elif role == 'dokter':
                    with connection.cursor() as dr:
                        dr.execute("select * from dokter where username=%s", [username])
                        hasil = dr.fetchone()
                        if hasil:
                            request.session['role'] = role
                            return redirect('login:home')

                elif role == 'pasien':
                    with connection.cursor() as psn:
                        psn.execute("select * from pasien where username=%s", [username])
                        hasil = psn.fetchone()
                        if hasil:
                            request.session['role'] = role
                            return redirect('login:home')
                
            return render(request, 'login.html', {'errors' : 'username/password salah', 
                                                'username' : username, 
                                                'password' : password,
                                                'form' : form})
    return render(request, 'login.html', {'form' : form})

def register(request):
    return render(request,'register.html')

def regisPasien(request):
    form = PasienForm
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login:fiturlogin')
    return render(request, 'akunPasien.html', {'form' : form})

def regisDoctor(request):
    form = DokterForm
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login:fiturlogin')
    return render(request, 'akunDoctor.html', {'form' : form})

def regisAdm(request):
    form = AdminForm
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login:fiturlogin')
    return render(request, 'akunAdministrator.html', {'form' : form})


#def fiturlogin(request):
    #if request.method == "POST":
        #form = AuthenticationForm(data = request.POST)
        #if form.is_valid():
            #user = form.get_user()

            #login(request, user)
            #return redirect('login:show')
    #else:
        #form = AuthenticationForm()
    #return render(request, "login.html", {"form" : form })


#def register(request):
    #if request.method == 'POST':
        #form_regis = registerForm(request.POST)
        #if form_regis.is_valid():
            #x = form_regis.save(commit = False)
            #x.first_name = form_regis.cleaned_data.get('first_name')
            #x.last_name = form_regis.cleaned_data.get('last_name')
            #x.save()

            #address1 = form_regis.cleaned_data.get('address')
            #number1 = form_regis.cleaned_data.get('number')
            #user_medikaGo = loginMedikaGo(address = address1, number = number1)
            #user_medikaGo.user = x
            #user_medikaGo.save()

            #x.save()
            #return redirect('login:fiturlogin')
    #else:
        #form_regis = registerForm()
    #return render(request, "register.html", {'form': form_regis})

#def show(request):
    #username_login = request.user.username
    #return render(request,'show.html',{'name' : username_login, "navbar" : True})