from django.urls import path
from . import views

#url for app

app_name = 'login'

urlpatterns = [
    path('',views.fiturlogin, name='fiturlogin'),
    path('register/', views.register, name='register'),
    path('regisPasien/', views.regisPasien, name='pasien'),
    path('regisDoctor/', views.regisDoctor, name='doctor'),
    path('regisAdministrator/', views.regisAdm, name='admin'),
    path('logout/',views.logout,name='logout'),
    path('home/',views.home,name='home')
]