from django.shortcuts import redirect, HttpResponse
from functools import wraps

def admin_only(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if not request.session.get('role', False):
            return redirect('login:fiturlogin')

        if request.session.get('role') == 'admin':
            return function(request, *args, **kwargs)
        return HttpResponse('sorry, you don\'t have permission')
    return wrap

def authors_only(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        if request.session.get('role') == 'pasien' or request.session.get('role') == 'dokter' or request.session.get('role') == 'admin':
            return function(request, *args, **kwargs)
        return redirect('login:fiturlogin')
    return wrap
