from django import forms
from datetime import datetime
from django.db import connection
from hashlib import sha3_512 as sha

class LoginForm(forms.Form):
    attrs = {'class' : 'form-control'}
    choices = [
        ('pasien', 'Pasien'),
        ('admin', 'Admin'),
        ('dokter', 'Dokter'),
    ]

    username = forms.CharField(label='Username', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='Password', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    role = forms.CharField(label='Role', required=True, max_length=255, widget=forms.Select(choices=choices, attrs=attrs))


class DokterForm(forms.Form):
    attrs = {'class' : 'form-control'}
    username = forms.CharField(
        label='Username', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(
        label='Password', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    no_identitas = forms.CharField(
        label='No Identitas', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(
        label='Nama Lengkap', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(
        label='Tanggal Lahir', widget=forms.SelectDateWidget(attrs=attrs))
    email = forms.CharField(
        label='Email', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    alamat = forms.CharField(
        label='Alamat', required=True, widget=forms.Textarea(attrs=attrs))
    no_sip = forms.CharField(
        label='No SIP', required=True, max_length=20, widget=forms.TextInput(attrs=attrs))
    spesialisasi = forms.CharField(
        label='Spesialisasi', required=True, max_length=20, widget=forms.TextInput(attrs=attrs))

    def save(self, commit=True):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        no_identitas = self.cleaned_data.get('no_identitas')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')
        tanggal_lahir = self.cleaned_data.get('tanggal_lahir')
        email = self.cleaned_data.get('email')
        alamat = self.cleaned_data.get('alamat')
        no_sip = self.cleaned_data.get('no_sip')
        spesialisasi = self.cleaned_data.get('spesialisasi')

        jumlah=0
        with connection.cursor() as hitung:
            hitung.execute("SELECT COUNT(*) FROM DOKTER")
            jumlah += hitung.fetchone()[0]

        id_dokter = 0
        if jumlah < 1:
            id_dokter = 1
        elif jumlah >=1:
            id_dokter = 1 + jumlah

        # save to the database postgresql
        with connection.cursor() as dokter:
            dokter.execute('insert into pengguna values (%s, %s, %s, %s, %s, %s, %s)', [email, username, password, nama_lengkap, no_identitas, tanggal_lahir, alamat])
            dokter.execute('insert into dokter values (%s, %s, %s, %s)', [id_dokter, username, no_sip, spesialisasi])

class AdminForm(forms.Form):
    attrs = {'class' : 'form-control'}
    username = forms.CharField(
        label='Username', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(
        label='Password', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    no_identitas = forms.CharField(
        label='No Identitas', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(
        label='Nama Lengkap', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(
        label='Tanggal Lahir', widget=forms.SelectDateWidget(attrs=attrs))
    email = forms.CharField(
        label='Email', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    alamat = forms.CharField(
        label='Alamat', required=True, widget=forms.Textarea(attrs=attrs))

    def save(self, commit=True):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        no_identitas = self.cleaned_data.get('no_identitas')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')
        tanggal_lahir = self.cleaned_data.get('tanggal_lahir')
        email = self.cleaned_data.get('email')
        alamat = self.cleaned_data.get('alamat')

        jumlah=0
        with connection.cursor() as hitung:
            hitung.execute("SELECT COUNT(*) FROM ADMINISTRATOR")
            jumlah += hitung.fetchone()[0]

        no_pegawai = 0
        if jumlah < 1:
            no_pegawai = 1
        elif jumlah >=1:
            no_pegawai = 1 + jumlah

        kode_rs = 105

        # save to the database postgresql
        with connection.cursor() as admin:
            admin.execute('insert into pengguna values (%s, %s, %s, %s, %s, %s, %s)', [email, username, password, nama_lengkap, no_identitas, tanggal_lahir, alamat])
            admin.execute('insert into dokter values (%s, %s, %s)', [no_pegawai, username, kode_rs])

class PasienForm(forms.Form):
    attrs = {'class' : 'form-control'}
    username = forms.CharField(
        label='Username', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(
        label='Password', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    no_identitas = forms.CharField(
        label='No Identitas', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    nama_lengkap = forms.CharField(
        label='Nama Lengkap', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    tanggal_lahir = forms.DateField(
        label='Tanggal Lahir', widget=forms.SelectDateWidget(attrs=attrs))
    email = forms.CharField(
        label='Email', required=True, max_length=255, widget=forms.TextInput(attrs=attrs))
    alamat = forms.CharField(
        label='Alamat', required=True, widget=forms.Textarea(attrs=attrs))
    alergi = forms.CharField(
        label='Alergi', required=True, widget=forms.TextInput(attrs=attrs))

    def save(self, commit=True):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        no_identitas = self.cleaned_data.get('no_identitas')
        nama_lengkap = self.cleaned_data.get('nama_lengkap')
        tanggal_lahir = self.cleaned_data.get('tanggal_lahir')
        email = self.cleaned_data.get('email')
        alamat = self.cleaned_data.get('alamat')
        alergi = self.cleaned_data.get('alergi')

        jumlah=0
        with connection.cursor() as hitung:
            hitung.execute("SELECT COUNT(*) FROM PASIEN")
            jumlah += hitung.fetchone()[0]

        no_rekam_medis = 0
        if jumlah < 1:
            no_rekam_medis = 301
        elif jumlah >=1:
            no_rekam_medis = 301 + jumlah

        nama_asuransi = 'ASURANSI A'

        # save to the database postgresql
        with connection.cursor() as pasien:
            pasien.execute('insert into pengguna values (%s, %s, %s, %s, %s, %s, %s)', [email, username, password, nama_lengkap, no_identitas, tanggal_lahir, alamat])
            pasien.execute('insert into pasien values (%s, %s, %s)', [no_rekam_medis, username, nama_asuransi])



#from django.db import models
# from django import forms
#from .models import loginMedikaGo
#from django.contrib.auth.models import User
#from django.contrib.auth.forms import UserCreationForm

#class registerForm(UserCreationForm):
    #first_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    #last_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    #address = forms.CharField(max_length=100, required=False, help_text='Please enter your address')
    #number = forms.IntegerField(required=False, help_text='Please enter your number')

    #class Meta:
        #model = User
        #fields = ('first_name', 'last_name', 'username','address','number', 'password1', 'password2', )

