from django.urls import path
from . import views

#url for app

app_name = 'create_obat'

urlpatterns = [
    path('',views.createObat, name='createObat'),
    path('daftarObat/', views.daftarObat, name='daftarObat'),
    path('updateObat/', views.updateObat, name='update'),
    path('deleteObat/', views.deleteObat, name='delete')
]