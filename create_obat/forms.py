from django import forms
from django.forms import widgets
from django.db import connection

class FormObat(forms.Form):
    kode =forms.CharField(
        label = 'Kode', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Kode Obat"}),
    )
    stok =forms.IntegerField(
        label = 'Stok',
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Stock Obat"}),
    )

    harga =forms.IntegerField(
        label = 'Harga', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Harga Obat"}),
    )

    komposisi =forms.CharField(
        label = 'Komposisi', required= False,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),
    )

    bentuk_sediaan =forms.CharField(
        label = 'Bentuk Sediaan', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Bentuk Obat"}),
    )

    merk_dagang =forms.CharField(
        label = 'Merk Dagang', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Merk Obat"}),
    )

    def save(self, commit=True):
        kode = self.cleaned_data.get('kode')
        stok = self.cleaned_data.get('stok')
        harga = self.cleaned_data.get('harga')
        komposisi = self.cleaned_data.get('komposisi')
        bentukSediaan = self.cleaned_data.get('bentuk_sediaan')
        merkDagang = self.cleaned_data.get('merk_dagang')

        with connection.cursor() as cursor:
            cursor.execute('INSERT into OBAT values (%s, %s, %s, %s, %s, %s)', [kode, stok, harga, komposisi, bentukSediaan,merkDagang])

    class Meta:
        # model = Obat
        fields= ('kode','stok','harga','komposisi','bentuk_sediaan','merk_dagang')

class FormUpdateObat(forms.Form):
    kode =forms.CharField(
        label = 'Kode', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'autocomplete':'off', 'readonly':'true'}),
    )
    stok =forms.IntegerField(
        label = 'Stok',
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Stock Obat"}),
    )

    harga =forms.IntegerField(
        label = 'Harga', required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Harga Obat"}),
    )

    komposisi =forms.CharField(
        label = 'Komposisi', required= False,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm'}),
    )

    bentuk_sediaan =forms.CharField(
        label = 'Bentuk Sediaan', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Bentuk Obat"}),
    )

    merk_dagang =forms.CharField(
        label = 'Merk Dagang', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Merk Obat"}),
    )
    
    def edit(self, commit=True, kode=None):
        stok2 = self.cleaned_data.get('stok')
        harga2 = self.cleaned_data.get('harga')
        komposisi2 = self.cleaned_data.get('komposisi')
        bentukSediaan2 = self.cleaned_data.get('bentuk_sediaan')
        merkDagang2 = self.cleaned_data.get('merk_dagang')

        with connection.cursor() as cursor:
            cursor.execute('UPDATE OBAT SET stok =%s,harga=%s,komposisi=%s,bentuk_sediaan=%s,merk_dagang=%s where kode =%s',
            [stok2, harga2, komposisi2, bentukSediaan2,merkDagang2,kode])



    class Meta:
        # model = Obat
        fields= ('kode','stok','harga','komposisi','bentuk_sediaan','merk_dagang')
      