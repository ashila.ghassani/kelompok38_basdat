from django.shortcuts import render,redirect
from .forms import FormObat, FormUpdateObat
from django.http import HttpResponseRedirect
from django.db import connection

# Create your views here.
def createObat(request):
    form = FormObat
    if request.method=="POST":
        form= form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('create_obat:daftarObat')            
            
    return render(request,'createObat.html', {'form' : form})


def daftarObat(request):
    aso = ''
    with connection.cursor() as obat:
        obat.execute("SELECT * FROM OBAT ORDER BY kode ASC")
        aso = obat.fetchall()
    
    return render(request, 'daftarObat.html',{'aso' : aso})

def updateObat(request):
    formUpdate = FormUpdateObat
    if request.method == 'POST':
        
        if request.POST.get('edit') == 'true':
            kodenya = request.POST.get('kodeobt')
          
            with connection.cursor() as obat:
                obat.execute('SELECT * from OBAT where kode =%s', [kodenya])
                obat = obat.fetchone()
                
                dct_of_obat = {
                    'kode' : obat[0],
                    'stok' : obat[1],
                    'harga' : obat[2],
                    'komposisi' : obat[3],
                    'bentuk_sediaan' : obat[4],
                    'merk_dagang' : obat[5],
                }

                formUpdate = formUpdate(dct_of_obat)

                return render(request, 'updateObat.html', {'formUpdate' : formUpdate, 'kode_obat' : obat[0]})
        else:
            formUpdate = formUpdate(request.POST)
            if formUpdate.is_valid():
                formUpdate.edit(kode = request.POST.get('kode_obat'))
                return redirect('create_obat:daftarObat')

    return render(request, 'updateObat.html', {'formUpdate' : formUpdate})


def deleteObat(request):
    if request.method == 'POST':
        kode = request.POST.get('kode')
        stok = request.POST.get('stok')
        harga = request.POST.get('harga')
        komposisi = request.POST.get('komposisi')
        bentukSediaan = request.POST.get('bentuk_sediaan')
        merkDagang = request.POST.get('merk_dagang')

        if kode:
            with connection.cursor() as admin:
                admin.execute('delete from obat where kode =%s and stok=%s and harga=%s and komposisi=%s and bentuk_sediaan=%s and merk_dagang=%s', [kode, stok, harga, komposisi, bentukSediaan, merkDagang])
    return redirect('create_obat:daftarObat')
