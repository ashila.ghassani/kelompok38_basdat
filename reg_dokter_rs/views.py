from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormDokterRS
from django.db import connection

# Create your views here.
def regis(request):
    form = FormDokterRS
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('dokterrsdata')
    return render(request, 'regis.html', {'form' : form})

def dokterrsdata(request):
    asd = ''
    with connection.cursor() as koders:
        koders.execute("SELECT DISTINCT kode_rs, STRING_AGG (id_dokter, '') FROM dokter_rs_cabang GROUP BY kode_rs ORDER BY kode_rs ASC")
        asd = koders.fetchall()
    
    return render(request, 'datadokterrs.html',{'asd' : asd})

def delete_dokrs(request):
    if request.method == 'POST':
        kodersnya = request.POST.get('koders')
        iddoknya = request.POST.get('iddok')
        if iddoknya:
            with connection.cursor() as admin:
                admin.execute('delete from dokter_rs_cabang where id_dokter=%s and kode_rs=%s', [iddoknya, kodersnya])
    return redirect('dokterrsdata')



def update_dokrs(request):
    if request.method == 'GET':
        return redirect('dokterrsdata')
    form = FormDokterRS
    if request.method == 'POST':
        
        if request.POST.get('edit') == 'true':
            kodersnya = request.POST.get('koders')
            iddoknya = request.POST.get('iddok')
            with connection.cursor() as dokter:
                dokter.execute('select * from dokter_rs_cabang where id_dokter=%s and kode_rs=%s', [iddoknya, kodersnya])
                dokter = dokter.fetchone()
                
                dct_of_dokter = {
                    'id_dokter' : dokter[0],
                    'kode_rs' : dokter[1],
                }

                form = form(dct_of_dokter)

                return render(request, 'updatedrrs.html', {'form' : form, 'idd' : dokter[0], 'krs' : dokter[1]})
        else:
            form = form(request.POST)
            if form.is_valid():
                form.edit(id_dokter=request.POST.get('idd'), kode_rs=request.POST.get('krs'))
                return redirect('dokterrsdata')
    return render(request, 'updatedrrs.html', {'form' : form})
