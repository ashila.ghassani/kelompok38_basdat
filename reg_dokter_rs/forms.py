from django import forms
from django.forms import widgets
from datetime import datetime
from django.db import connection
from hashlib import sha3_512 as sha

class FormDokterRS(forms.Form):
    id_dokter = forms.ChoiceField(
        label='ID Dokter',
        required=True,
        choices=[('1', '1'),
                ('2', '2'),
                ('3', '3'),
                ('4', '4'),
                ('5', '5'),
                ('6', '6'),
                ('7', '7')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )
    kode_rs = forms.ChoiceField(
        label='Kode RS',
        required=True,
        choices=[(('101', '101')),
                ('102', '102'),
                ('103', '103'),
                ('104', '104'),
                ('105', '105')
                ],
        widget = forms.Select(attrs = {'class' : 'form-control'})
    )

    def save(self, commit=True):

        id_dokter = self.cleaned_data.get('id_dokter')
        kode_rs = self.cleaned_data.get('kode_rs')

        with connection.cursor() as dokter:
            dokter.execute('insert into dokter_rs_cabang values (%s, %s)', [id_dokter, kode_rs])

    def edit(self, commit=True, id_dokter=None, kode_rs=None):
        if not (id_dokter and kode_rs):
            return

        iddokter = self.cleaned_data.get('id_dokter')
        koders = self.cleaned_data.get('kode_rs')

        with connection.cursor() as dokter:
            dokter.execute('update dokter_rs_cabang set id_dokter=%s, kode_rs=%s where id_dokter=%s and kode_rs=%s', [iddokter, koders, id_dokter, kode_rs])

    class Meta:
        # model = DokterRS
        fields= ('iddokter','koders')
