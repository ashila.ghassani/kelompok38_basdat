from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('', create_rs_cabang, name='create_rs_cabang'),
    path('rud_rs_cabang/', rud_rs_cabang, name='rud_rs_cabang'),
]
